// ==UserScript==
// @name         Video Focuser
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Default focus on video players for sites that don't do that for some reason
// @author       Inityx
// @match        https://*/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    document.getElementById("xplayer__video")?.focus();
})();
